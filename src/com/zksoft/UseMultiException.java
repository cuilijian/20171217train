package com.zksoft;

public class UseMultiException extends Exception {
	static String str1;
	static String str2;

	public static void main(String[] args) {
		int i1 = 0;
		int i2 = 0;
		str1 = "abc";
		str2 = "0";
		String strResult = new String();
		try {
			read(str1, str2);
			i1 = Integer.parseInt(str1);
			i2 = Integer.parseInt(str2);
			int result = i1 / i2;
			strResult = String.valueOf(result);

		} catch (NumberFormatException e) {
			strResult = "错误的数字：" + e.getMessage();
		} catch (ArithmeticException e) {
			strResult = "被0除错误：" + e.getMessage();
		}
		System.out.println(strResult);
	}

	private static void read(String s1, String s2) {
		str1 = s1.trim();
		str2 = s2.trim();
		if (str2.equals("0")) {
			try {
				throw (new UserDefineException("本系统0不能为被除数"));
			} catch (UserDefineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

class UserDefineException extends Exception {
	public UserDefineException(String msg) {
		super(msg);
	}
}
