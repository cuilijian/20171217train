package com.zksoft;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class exceptionExample {

	/**
	 * 模拟Trasaction=true情况，智能卡操作
	 * 
	 * @throws SQLException
	 */
	public void excepTransaction() {
		Connection conn = null;
		try {

			// 首先判断智能卡真假
			if (ifCardTrue(conn) && ifPosTrue(conn)) {
				removeMoney(conn, 3000);
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (CardTrueException cte) {
			return "智能卡有问题，请联系服务人员";
		} catch (PosTrueException cte) {
			return "POS机有问题，请联系服务人员";
		} catch (MoneyOperationException cte) {
			return "转账出现错误";
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private Boolean ifCardTrue(Connection conn) {
		Boolean cardExist = false;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select asdf asdf ");
			if (rs != null && rs.next()) {
				cardExist = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cardExist;
	}

	private Boolean ifPosTrue(Connection conn) {
		return true;
	}

	private void removeMoney(Connection conn, int number) {

	}
}
