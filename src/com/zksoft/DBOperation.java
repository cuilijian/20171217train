package com.zksoft;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

public class DBOperation {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:mysql://127.0.0.1:3306/demo";
		String user = "root";
		String password = "admin";
		String uname = "test15";
		String pass = "000001";
		Connection conn = DriverManager.getConnection(url, user, password);
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			Statement stmt = conn.createStatement();
			// String sql = "select nickname from users";
			// ResultSet rs = stmt.executeQuery(sql);
			//
			// while (rs.next()) {
			// System.out.println(rs.getString("nickname"));
			// }
			// rs.close();
			if (login(stmt, uname, pass)) {
				System.out.println("用户登录成功");
			} else {
				System.out.println("用户登录失败");
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			conn.close();
			e.printStackTrace();
		}
	}

	private static boolean login(Statement stmt, String username, String password) throws SQLException {
		boolean ifLogin = false;
		String sql = "select * from users where login_pwd ='" + password + "' and login_id ='" + username + "'";
		ResultSet rs = stmt.executeQuery(sql);
		if (rs.next()) {
			ifLogin = true;
		}
		return ifLogin;
	}

	private static boolean login(PreparedStatement pstmt, String username, String password) throws SQLException {
		boolean ifLogin = false;
		String sql = "select * from users where login_pwd =? and login_id =?";

		ResultSet rs = pstmt.executeQuery(sql);
		pstmt.setString(1, password);
		pstmt.setString(2, username);
		if (rs.next()) {
			ifLogin = true;
		}
		return ifLogin;
	}
}
