package com.zksoft;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlException {

	public static void main(String[] args) {
		myMethod();
	}

	private static void myMethod() {
		try {
			URL urlName = new URL("111");
			System.out.println(urlName.getHost());
			// getAppletContext().showDocument(urlName,"right");
		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
		}
	}

}
